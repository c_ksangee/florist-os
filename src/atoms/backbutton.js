import React from 'react';
import ArrowBackIosOutlinedIcon from '@material-ui/icons/ArrowBackIosOutlined';

const BackButton= ( ) => {

    function backButtonClick(){
        window.history.back();
    }
   
    return (
            <button type="button" onClick={ backButtonClick } className="btn text-color-black">
                {/* <i className="arrow left  "></i> */}
                <ArrowBackIosOutlinedIcon />
                <span className='backText text-color-black'>Back</span>
            </button>
    )
}

export default BackButton;