import React from "react";
import { Link } from "react-router-dom";
import HomeOutlinedIcon from '@material-ui/icons/HomeOutlined';

function HomeBtn() {
  return (
    <Link to="/">
      <div className="home-btn text-color-black">
        {/* <i className="mdi mdi-home border-blue"></i> */}
        <HomeOutlinedIcon />
      </div>
    </Link>
  );
}

export default HomeBtn;
