import React from "react";
import FtdLogo from "static/images/ftd.svg";

function HorizontalNav() {
  return (
    <nav className="navbar p-0 fixed-top d-flex flex-row d-print-none">
      <div className="navbar-brand-wrapper d-flex d-lg-none align-items-center justify-content-center">
        <a className="navbar-brand brand-logo-mini" href="index.html">
          <img src={FtdLogo} alt="ftd logo" />
        </a>
      </div>
      <div className="navbar-menu-wrapper flex-grow d-flex align-items-stretch">
        <button
          className="navbar-toggler navbar-toggler align-self-center"
          type="button"
          data-toggle="minimize"
        >
          <span className="mdi mdi-menu"></span>
        </button>
        <p className="text-center col-11 mt-4">
          Welcome <span className="font-weight-bold">William</span>
          {", "}
          75-0053AA
        </p>
        <button
          className="navbar-toggler navbar-toggler-right d-lg-none align-self-center"
          type="button"
          data-toggle="offcanvas"
        >
          <span className="mdi mdi-format-line-spacing"></span>
        </button>
      </div>
    </nav>
  );
}

export default HorizontalNav;
